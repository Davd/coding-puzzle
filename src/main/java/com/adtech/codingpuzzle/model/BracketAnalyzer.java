/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adtech.codingpuzzle.model;

/**
 *
 * @author davd
 */
public abstract class BracketAnalyzer implements BracketRules {
    
    protected String sequence;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
    
    public BracketAnalyzer() {
    }
    
    public BracketAnalyzer(String sequence) {
        this.sequence = sequence;
    }
}
