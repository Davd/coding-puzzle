/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adtech.codingpuzzle.model;

/**
 *
 * @author davd
 */
public class NoInputException extends Exception {
    
    private String message;
    
    @Override
    public String getMessage() {
        return message;
    }
    
    public NoInputException(String message) {
        this.message = message;
    }
}
