/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adtech.codingpuzzle.model;

import com.adtech.codingpuzzle.Bundle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author davd
 */
public class BracketReader {

    private volatile Map<String, Boolean> results = new HashMap<>();
    private List<String> input;

    public void scanInput() {
        input = new LinkedList<>();

        Scanner scan = new Scanner(System.in);
        String line;
        while ((line = scan.nextLine()) != null && !line.isEmpty()) {
            input.add(line);
        }
    }

    public synchronized void runAnalysis() throws NoInputException, InterruptedException {
        if (input == null) {
            throw new NoInputException(Bundle.MODEL.getString("bracketreader_runanalysis_ni"));
        }

        ExecutorService executor = Executors.newCachedThreadPool();
        List<Future> tasks = new ArrayList<>();

        for (Iterator<String> sequenceIt = input.iterator(); sequenceIt.hasNext();) {
            final String sequence = sequenceIt.next();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    BracketAnalyzer ba = new BasicBracketAnalyzer();
                    ba.setSequence(sequence);
                    results.put(sequence, ba.valid());
                }
            };
            tasks.add(executor.submit(r));
        }
        
        System.out.print(Bundle.MODEL.getString("bracketreader_runanalysis_wait"));
        while (!finishedTasks(tasks)) {
            System.out.print(".");
            this.wait(1000);
        }
        System.out.println();
    }
    
    private boolean finishedTasks(List<Future> tasks) {
        for (Iterator<Future> fIt = tasks.iterator(); fIt.hasNext();) {
            Future f = fIt.next();
            if (!f.isDone()) {
                return false;
            }
        }
        return true;
    }

    public Map<String, Boolean> getResults() throws NotYetAnalyzedException {
        if (results == null) {
            throw new NotYetAnalyzedException(Bundle.MODEL.getString("bracketreader_getresults_nya"));
        }
        return results;
    }
}
