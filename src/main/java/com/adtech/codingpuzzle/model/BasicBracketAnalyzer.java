/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adtech.codingpuzzle.model;

import java.util.LinkedList;

/**
 *
 * @author davd
 */
public class BasicBracketAnalyzer extends BracketAnalyzer {

    private LinkedList<Character> openedBrackets = new LinkedList<>();
    private static final int OPENING_BRACKET = 0;
    private static final int CLOSING_BRACKET = 1;
    private static final int NOT_BRACKET = 2;

    public BasicBracketAnalyzer() {
    }

    public BasicBracketAnalyzer(String sequence) {
        super(sequence);
    }

    @Override
    public Boolean valid() {
        char[] sequenceChars = sequence.toCharArray();
        if (sequence.isEmpty()) {
            return false;
        }
        for (int c = 0; c < sequenceChars.length; c++) {
            char character = sequenceChars[c];
            int charType = bracketType(character);
            if (charType == OPENING_BRACKET) {
                boolean opened = openBracket(character);
                if (!opened) {
                    return false;
                }
            } else if (charType == CLOSING_BRACKET) {
                boolean closed = closeBracket(character);
                if (!closed) {
                    return false;
                }
            } else {
                return false;
            }
        }
        if (openedBrackets.isEmpty()) {
            return true;
        }
        return false;
    }

    private int bracketType(char b) {
        if (b == '(' || b == '[' || b == '{') {
            return OPENING_BRACKET;
        } else if (b == ')' || b == ']' || b == '}') {
            return CLOSING_BRACKET;
        }
        return NOT_BRACKET;
    }

    private boolean openBracket(char b) {
        if (openedBrackets.isEmpty()) {
            openedBrackets.add(b);
            return true;
        }
        char lastOpenedBracket = openedBrackets.getLast();
        if (lastOpenedBracket == '{') { // braces rules
            if (b != '[') {
                return false;
            }
        } else if (lastOpenedBracket == '(') { // parenthesis rules
            if (b != '{') {
                return false;
            }
        }
        openedBrackets.add(b);
        return true;
    }

    private boolean closeBracket(char b) {
        if (openedBrackets.isEmpty()) {
            return false;
        }
        char lastOpenedBracket = openedBrackets.getLast();
        if (lastOpenedBracket == '{' && b == '}') {
            openedBrackets.removeLast();
            return true;
        } else if (lastOpenedBracket == '(' && b == ')') {
            openedBrackets.removeLast();
            return true;
        } else if (lastOpenedBracket == '[' && b == ']') {
            openedBrackets.removeLast();
            return true;
        }
        return false;
    }
}
