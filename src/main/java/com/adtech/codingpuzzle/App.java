package com.adtech.codingpuzzle;

import com.adtech.codingpuzzle.model.BracketReader;
import com.adtech.codingpuzzle.model.NoInputException;
import com.adtech.codingpuzzle.model.NotYetAnalyzedException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);

        try {
            BracketReader br = new BracketReader();
            br.scanInput();
            br.runAnalysis();
            Map<String, Boolean> results = br.getResults();
            // print results
            for (Iterator<String> keysIt = results.keySet().iterator(); keysIt.hasNext();) {
                String key = keysIt.next();
                Boolean val = results.get(key);
                System.out.println(val + ": " + key);
            }
        } catch (NoInputException | NotYetAnalyzedException | InterruptedException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
