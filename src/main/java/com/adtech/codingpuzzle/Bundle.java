/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adtech.codingpuzzle;

import java.util.ResourceBundle;

/**
 *
 * @author davd
 */
public enum Bundle {
    
    MODEL;
    
    private static final String packageName = "com.adtech.codingpuzzle.resources.bundle";
    
    public String getString(String id) {
        return ResourceBundle.getBundle(packageName + "." + name().toLowerCase()).getString(id);
    }
}
